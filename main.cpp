#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<cmath>
#include<cstdlib>
#include<cstring>
#include<algorithm>

using namespace std;

class atom {
public:
  int id;
  int type;
  double x, y, z;
  double vx, vy, vz;
  double fx, fy, fz;
  double omegax, omegay, omegaz;
  double radius;

  atom()
  {
    this->id = 0;
    this->type = 0;
    this->x = 0.0;
    this->y = 0.0;
    this->z = 0.0;
    this->vx = 0.0;
    this->vy = 0.0;
    this->vz = 0.0;
    this->fx = 0.0;
    this->fy = 0.0;
    this->fz = 0.0;
    this->omegax = 0.0;
    this->omegay = 0.0;
    this->omegaz = 0.0;
    this->radius = 0.0;
  }
  ~atom()
  {
  }
};

int timestep;
vector < vector < vector < double > > > bindata;

/* Variable to read simulation box properties */
int numofatoms=0;
double xlo,xhi,ylo,yhi,zlo,zhi;
double boxx,boxy,boxz;
int nxbins, nybins;

void doit(char *inpfile, double binWidth)
{
  int i;
  int j;
  int k;
    
  /* Variables to store binwise properties */
  int N; // Number of particles per bin
  double V; //Area of bin
  double nu;
  double T;
  double f;
  double f2;
  double c;
  double Ma;
  double U;
  double ST;
 
  /* Variables to read per atom properties*/
  int id=0;
  int type=0;
  double x,y,z;
  double vx, vy, vz;
  double fx, fy, fz;
  double omegax, omegay, omegaz;
  double radius;

  string word;
  string temp;

  /* File Streams */ 
  ifstream fin;
  
  /* Vector to store all the data */
  vector < atom* > atoms;
  vector < vector < vector < int > > > bins;
  
  fin.open(inpfile);
  if(fin.bad()) 
    cout<<"Error opening the file."<<endl;

  cout<<"Input file :"<<inpfile<<endl<<"Reading file..."<<endl;
  while(!fin.eof())
    {
      fin>>word;
      if(strcmp(word.c_str(),"ITEM:")==0)
	{
	  fin>>word;		
	  if(strcmp(word.c_str(),"TIMESTEP")==0)
	    {
	      fin>>temp;
	    }
	  else if(strcmp(word.c_str(),"NUMBER")==0)
	    {
	      fin>>temp>>temp;
	      fin>>numofatoms;	
	      for(i = 0;i < numofatoms;i++)
		{
		  atoms.push_back(new atom());
		}
	    }else if(strcmp(word.c_str(),"BOX")==0)
	    {
	      fin>>temp>>temp>>temp>>temp;
	      fin>>xlo>>xhi>>ylo>>yhi>>zlo>>zhi;
	      boxx=xhi-xlo;
	      boxy=yhi-ylo;
	      boxz=zhi-zlo;
	      nxbins = boxx / binWidth + 1;
	      nybins = boxy / binWidth + 1;
	      for(i = 0;i < nxbins;i++)
		{
		  bins.push_back(vector < vector < int > >());
		  for(j = 0;j < nybins;j++)
		    {
		      bins[i].push_back(vector < int >());
		    }
		}
	      if(timestep == 0)
		{
		  for(i = 0;i < nxbins;i++)
		    {
		      bindata.push_back(vector < vector < double > > ());
		      for(j = 0;j < nybins;j++)
			{
			  bindata[i].push_back(vector < double > ());
			  for(k = 0;k < 8;k++)
			    {
			      bindata[i][j].push_back(0.0);
			    }
			}
		    }
		}
	    }
	  else if(strcmp(word.c_str(),"ATOMS")==0)
	    {
	      //id type x y z vx vy vz fx fy fz omegax omegay omegaz radius
	      fin>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp;
	      id = 0;
	      for(i=0;i<numofatoms;i++)
		{
		  fin>>temp>>type>>x>>y>>z>>vx>>vy>>vz>>fx>>fy>>fz>>omegax>>omegay>>omegaz>>radius;
		  atoms[id]->type = type;
		  atoms[id]->x = x;
		  atoms[id]->y = y;
		  atoms[id]->z = z;
		  atoms[id]->vx = vx;
		  atoms[id]->vy = vy;
		  atoms[id]->vz = vz;
		  atoms[id]->fx = fx;
		  atoms[id]->fy = fy;
		  atoms[id]->fz = fz;
		  atoms[id]->omegax = omegax;
		  atoms[id]->omegay = omegay;
		  atoms[id]->omegaz = omegaz;
		  atoms[id]->radius = radius;
		  id++;
		}

	      /* Sorting to bins */
	      for(i = 0; i < atoms.size();i++)
		{
		  bins[(int)((atoms[i]->x - xlo) / binWidth)][(int)((atoms[i]->y - ylo) / binWidth)].push_back(i);
		}

	      /* Calculating binwise properties and output to file*/
	      for(i = 0;i < nxbins;i++)
	      {
		for(j = 0;j < nybins;j++)
		  {
		    N = bins[i][j].size();
		    V = binWidth * binWidth;
		    ST = 0.0;
		    c = 0.0;
		    Ma = 0.0;
		    U = 0.0;
		    for(k = 0;k < N;k++)
		      {
			radius = atoms[bins[i][j][k]]->radius;
			vx = atoms[bins[i][j][k]]->vx;
			vy = atoms[bins[i][j][k]]->vy;
			U += vy;
			T = ((vx * vx)+((0.1 - vy) * (0.1 - vy))) / N;
			ST += T;
		      }
		    ST /= N;
		    nu = (N * 4.1905 * radius * radius * radius) / (V * 0.0008);
		    f = ((16.0 - (7.0 * nu)) / (16 * (1.0 - nu) * (1.0 - nu)));
		    f2 = ((7.0 * nu) - 25.0) / (16.0 * (nu - 1.0) * (nu - 1.0 * (nu - 1.0)));
		    c = sqrt(T * (f2 + ((f / nu) * (f / nu))));
		    Ma = (-U) / (N*c);

		    if(N==0)
		      {
			nu = 0.0;
			f = 0.0;
			ST = 0.0;
			c = 0.0;
			Ma = 0.0;
		      }
		    
		    bindata[i][j][0] += N;
		    bindata[i][j][1] += V;
		    bindata[i][j][2] += nu;
		    bindata[i][j][3] += f;
		    bindata[i][j][4] += f2;
		    bindata[i][j][5] += ST;
		    bindata[i][j][6] += c;
		    bindata[i][j][7] += Ma;
		    
		  }
	      }
	      timestep++;
	      /* Clean up arrays */
	      bins.clear();
	      for(i = 0;i < numofatoms;i++)
		{
		  delete atoms[i];
		}
	      atoms.clear();
	    }
	}
    }
  cout<<"Done...\n";	
  fin.close();
}

int main(int argc, char **argv)
{
  int i;
  int j;
  int k;
  double binWidth = 0.0;
  ofstream fout;
  char outfileName[256];
  
  if(argc>=3)
    {
      binWidth = atof(argv[1]);
      timestep = 0;
      for(i = 2;i < argc;i++)
	doit(argv[i], binWidth);
    }
  else
    cout<<"Arguments <bin-width> <input-file(s)>"<<endl;

  // Print output file
  strcpy(outfileName,"");
  strcat(outfileName,argv[2]);
  strcat(outfileName,".binwise");
  fout.open(outfileName,ios::out);

  fout<<"#Input file :"<<argv[2]<<endl;
  fout<<"#bin_x bin_y N V nu f f2 T c Ma\n";
  for(i = 0;i < nxbins;i++)
    {
      for(j = 0;j < nybins;j++)
	{
	  for(k = 0;k < 8;k++)	    
	    bindata[i][j][k] /= ((double) timestep);
	  
	  fout<<xlo + (((double) i) / ((double)nxbins)) * boxx + binWidth * 0.5<<"\t"<<ylo + (((double)j) / ((double)nybins)) * boxy + binWidth * 0.5<<"\t"<<bindata[i][j][0]<<"\t"<<bindata[i][j][1]<<"\t"<<bindata[i][j][2]<<"\t"<<bindata[i][j][3]<<"\t"<<bindata[i][j][4]<<"\t"<<bindata[i][j][5]<<"\t"<<bindata[i][j][6]<<"\t"<<bindata[i][j][7]<<endl;
	}
    }
  fout.close();
  return 0;
}
